\chapter{System analysis} 
\section{Trimming the model}
The main difference between the symbolic and manual linearizing is that for the first case both inputs at equilibrium point are zero. Other than that both methods deliver the same state space matrices. Without any further information about pump voltage and tank size constraints, scaling the system is impossible.

\section{System representation and analysis}
Calculating the poles and zeros in state space form is usually a nonfavoritable choice since non-observable or non-controllable poles and zeros can be included. This could be avoided by finding its minimal realization but this in its own would be very difficult to do by hand. That is why the transfer function $\mathbf{G}$(s) is used most of the times.
\subsubsection{Poles}
In state space the poles of the system are the eigenvalues of $\mathbf{A}$. This corresponds to the least common denominator of all non-identically zero minors of all orders of $\mathbf{G}$(s). Minors of order 1 are the four elements of the transfer function matrix.
Minor of order 2 is the determinant of $\mathbf{G}$(s) itself.
\subsubsection{Zeros}
Zeros are defined as the values of s for which $\mathbf{P}(s)$=
$\begin{bmatrix}
	s\mathbf{I}-\mathbf{A} & -\mathbf{B}  \\
	\mathbf{C} & \mathbf{D} 
\end{bmatrix}$ loses rank, resulting in zero output for some non-zero input.
On the other hand this zero polynomial z(s) corresponds to the greatest common divisor of all the numerators of all order-r minors of $\mathbf{G}(s)$ which are adjusted so that they have the pole polynomial $\Phi(s)$ as denominator. Where r is the normal rank of G(s). In our case r=2. A common mistake in Matlab is using tzero($\mathbf{G}(s)$). The command calculates the zeros from the state space, i.e. tzero(ss($\mathbf{G}(s)$)), which usually results in a non-minimal SS representation.


\subsubsection{Pole and zero-directions}
From the state space one uses the left and right eigenvectors of system matrix $\mathbf{A}$ in order to find the input and output pole direction vectors:
  \begin{equation}
  \begin{aligned}
 	\mathbf{u}_{p_i}=B^H\mathbf{q}_i \\
	\mathbf{y}_{p_i}=C\mathbf{t}_i
  \end{aligned}
  \label{task1:pol_directions}
 \end{equation}
 
On the other hand, the zero input and output directions are calculated from the singular value decomposition of $\mathbf{G}(s)$ at each zero, $z_i$, by taking the last singular vectors of $\mathbf{V}$ and $\mathbf{U}$ respectively.
\subsubsection{Minimum phase system}
Using the idea mentioned above we come up with the pole polynomial of $\mathbf{G}$(s): $\Phi(s)=(s+0.0159)(s+0.0111)(s+0.0419)(s+0.0333)$. They are the same as poles computed with Matlab. This result was expected since our system has 4 states.
Calculated as in \ref{task1:pol_directions} the input and output pol direction matrices $\mathbf{U}_p$ and $\mathbf{Y}_p$ are as following:
\begin{equation}
\mathbf{U}_p=\begin{bmatrix}
    0.7327  &  0.5969    &     0  &  1.0000 \\
	0.6805  &  0.8023  &  1.0000    &     0
\end{bmatrix}
\end{equation}

\begin{equation}
\mathbf{Y}_p=\begin{bmatrix}
	1  &   0  &  -1  &   0 \\
	0  &   1  &   0  &  -1
\end{bmatrix}
\end{equation}


The transmission zeros of our MIMO system are $0.0580$ and $-0.0172$ with their respective input and output vector direction matrices:
\begin{equation}
\mathbf{U}_z=\begin{bmatrix}
   -0.8302  &  0.6981\\
	-0.5575 & -0.7160
\end{bmatrix}
\end{equation}

\begin{equation}
\mathbf{Y}_z=\begin{bmatrix}
    0.4134  & -0.1543\\
	0.9105  &  0.9880
\end{bmatrix}
\end{equation}


\subsubsection{Non-minimum phase system}
For the non-minimum phase system we get the same poles as for the minimum phase system but with different directions: 

\begin{equation}
\mathbf{U}_{p_{non}}=\begin{bmatrix}
0.7327  &  0.5969    &     0   & 1.0000  \\
0.6805  &  0.8023  &  1.0000     &    0
\end{bmatrix}
\end{equation}

\begin{equation}
\mathbf{Y}_{p_{non}}=\begin{bmatrix}
   	0.4134  & -0.1543 \\
	0.9105  &  0.9880
\end{bmatrix}
\end{equation}

The main difference between two cases are the zeros. For this case we have $-0.0562$ and $0.0128$.
We expect the RHP zero to slow down the system compared to the minimal phase system. Another fact to be stated is that feedback control cannot move zeros and a RHP zero canceling would lead to an internally unstable system where the input signal u(t) of the system could get very big.
Their respective directions are the following:


\begin{equation}
\mathbf{U}_{z_{non}}=\begin{bmatrix}
  	 -0.8302  &  0.6981 \\
	-0.5575  & -0.7160
\end{bmatrix}
\end{equation}

\begin{equation}
\mathbf{Y}_{z_{non}}=\begin{bmatrix}
    0.4134  & -0.1543 \\
	0.9105  &  0.9880
\end{bmatrix}
\end{equation}

\section{Relative Gain Array (RGA)} \label{rga}
A desirable property of a decentralized control system is that it has integrity, that is, the closed-loop system should remain stable as subsystem controllers are brought in and out of service or when input saturates. This property can very good be observed from the RGA (Relative Gain Array). RGA gives the ratio of the open-loop and (partially) closed-loop gains, $\frac{g_{ij}}{g_{ij}}$. Large RGA elements indicate strong interactions. While pairing inputs and outputs two main rules have to be kept in mind: \newline \newline
\textbf{Rule no 1.} Avoid pairing corresponding to negative static gain. \newline
\textbf{Rule no 2.} Choose pairing close to 1 for the important frequency rang. \newline \newline
Watching the RGA of the minimal-phase system at steady-state in Fig.~\ref{fig:rga_dynamic}, according to the rule nr.1, that is to avoid negative static gains, leads to a diagonal pairing. As we can further see the situation stays the same for increasing frequency.
\newline
\newline
Observing the static RGA of the non-minimal phase system in Fig.~\ref{fig:rga_dynamic}, the same argument as above suggests off-diagonal pairing. However, at higher frequencies the diagonal elements are again closer to one. This makes the system very hardly controlled in general since both inputs influence both outputs similarly at the same time. A decision on pairing can be taken according to the crossover frequency. Assuming a cut-off frequency between 0.1 and 1 rad/s makes the off-diagonal pairing the favorite choice. An off-diagonal pairing is quite surprising from a physical point of view because it involves using the pump 2 to control the level in the first tank even though there is no direct connection between them. 
\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.5]{images/rga_dynamic}
	\caption{RGA values over frequency for min and non-min phase system}
	\label{fig:rga_dynamic}
\end{figure}

\section{Parametric uncertainty for nonlinearities}
While it is difficult to give exact approximation of the parametric uncertainties, we can certainly say that there is no uncertainty for the nonlinearities concerning the matrices B and C since the linearized matrices B and C coincide to the nonlinearized ones.

As for the matrix A, thinking back on the linearization process it is noticeable that every nonzero term of A is linearized in quite a similiar manner. We have the linearization of square-root function via first order Tailor expansion. Since square root function has good linear qualities we can approximate the nonlinearity uncertainty by parametrizing every non zero element $\mathbf{A}_{ij}$ such that it can take take values up to $10\%$ bigger or smaller than its nominal value %in low frequencies and increase up to 100\% for high frequencies
,i.e. $0.9<Ap_{ij}/A_{ij}<1.10$ for all nonzero elements of matrix A in both operating points.

\section{Singular values}
As it can be seen from the Fig.~\ref{fig:singular_values} the condition of the system is 3.5 at steady state and becomes smaller with growing frequency. This is an expected behaviour since as we saw in Quest.~\ref{rga}, RGA elements were relatively small. We can say that our system is relatively good conditioned.
Observing the Fig.~\ref{fig:singular_values} we notice a bandwidth of around $0.1 \frac{rad}{s}$ for the minimal phase system and $0.01\frac{rad}{s}$ for the non-min phase system. With this bandwidths we forecast long resting times, especially for the non-min phase system.
\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.52]{images/singular_values}
	\caption{}
	\label{fig:singular_values}
\end{figure}