\chapter{Decentralized control and uncertainty modeling}

Very often a decentralized controller as shown in Fig.~\ref{fig:decentralized_controller} is used to control a MIMO plant. Not only is it easier to design but it can be proved theoretically (Zames and Bensoussan, 1983)\cite{Sko05} that one may achieve perfect control of all outputs, provided the plant has no RHP-zeros. The design consists of two steps. Firstly a pairing of inputs and outputs has to be done, followed by a controller tuning for each pairing. This task aims to design a decentralized PI controller for our plant and evaluate its performance.%This will give us a feeling why in many cases an $H_{\infty}$ controller is preferred, instead.

\input{images/decentralised_controller}

\section{Decentralized controller based on linear and nonlinear models}
For both pairing decisions done in Quest.~\ref{rga}, a controller can be found which successfully stabilizes the plants. In order to show that, the Niederlinski Index(NI) is used, as shown on the assignment papers and in \cite{Teach_tank}.

With that said we now try to compute $K_1$, $K_2$, $T_1$ and $T_2$ in order to satisfy setting time $t_s < 45$ sec and overshoot $Mp<10\%$ for the linearized system on both operating points via a feedback control: 
\begin{equation}
\mathbf{K}=\begin{bmatrix}
 	K_1(1+\frac{1}{T_1s}) & 0 \\
	0  & K_2(1+\frac{1}{T_2s})
\end{bmatrix}.
\end{equation}
One can immediately see that it is very easy to find a PI controller for the min-phase operating point.

That is why we randomly choose following controller:
\begin{equation}
\mathbf{K_{min}}=10(1+\frac{1}{10s})\begin{bmatrix}
	1 & 0 \\
	0  & 1
\end{bmatrix}.
\end{equation}
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.62\textwidth]{images/decentralised_controller_1_min_phase}
	\caption{Step response of individually controlled part-systems $g_{11}$ and $g_{22}$}
	\label{fig:decentralized_controller_1}
\end{figure}

In contrast to that, it is very difficult to design a decentralized PI controller that satisfies the given performance for non minimal operating point. After tuning the parameters through the bode diagrams of the independent SISO systems and a lot of trial and error, the off-diagonal pairing led to the controller:

\begin{equation}
\mathbf{K_{non-min}}=\begin{bmatrix}
	0 & 0.39(1+\frac{1}{125s})  \\
	0.65(1+\frac{1}{200s})  & 0
\end{bmatrix}.
\label{eq:non-min_decentral_controller}
\end{equation}


\begin{figure}[!h]
		\centering
	\includegraphics[width=0.62\textwidth]{images/decentralised_controller_2_non-min_phase}
	\caption{Step response of individually controlled part-systems $g_{11_{non}}$ and $g_{22_{non}}$}
	\label{fig:decentralized_controller_2}
\end{figure}
\newpage
In Fig.~\ref{fig:decentralized_controller_2} we can once again clearly see that a RHP zero slows the system down. It is too impossible to move a RHP zero with feedback. 
That is why the performance conditions could be satisfied only for the min phase system.

The simulations for the linear- and nonlinear  model are shown in Fig~\ref{fig:step_response_decentral_min} and Fig~\ref{fig:step_response_decentral_non_min} for the min phase and non-min phase system respectively. We observe similar behaviour on the minimal phase operating point. The nonlinear non-min phase system on the other hand, has no overshoot and tends to be slower compared to the linearized model, as it takes almost 400 seconds longer to settle.

\begin{figure}[!h]
	\includegraphics[width=1 \textwidth,left]{images/step_response_decentral_min}
	\caption{Step response of minimal phase system with decentralized PI controller}
	\label{fig:step_response_decentral_min}
\end{figure}

\begin{figure}[!h]
	\includegraphics[width=1 \textwidth,left]{images/step_response_decentral_non_min}
	\caption{Step response of non-minimal phase system with decentralized PI controller}
	\label{fig:step_response_decentral_non_min}
\end{figure}

 

%\begin{figure}[!h]
%	\subfloat[Nonlinear system in min phase operating point]{%
%		\includegraphics[height=5cm, width=0.47\textwidth]{images/nonlinear_decentral1}}
%		\label{fig:decental_nonlinear1}
%	\hfill
%	\subfloat[Nonlinear system in non-min phase operating point]{%
%		\includegraphics[height=5cm,width=0.47\textwidth]{images/nonlinear_decentral2}
%		\label{fig:decental_nonlinear2}
%	}
%	\label{fig:nonlinear_decentral12}
%\end{figure}

\clearpage
\section{Model uncertainty and weighting function}
In this question we have to approximate the parametric uncertainty through dynamic uncertainty. Since no further information is given we decide to approximate it with multiplicative output uncertainty weight and normalized full perturbation,

\begin{equation}
	\begin{aligned}
		\Pi_O: \mathbf{G_p}(s)=(\mathbf{I}+w_O(s)\Delta_O(s))\mathbf{G}(s),  \text{\quad} ||\Delta_O(s)||_{\infty}\leq 1
	\end{aligned}
\end{equation}

That is done by sampling the augmented plant and computating the magnitude of multiplicative output uncertainty as:

\begin{equation}
\begin{aligned}
\l_O(w)=\max\overline{\sigma}((\mathbf{G_p}-\mathbf{G})\mathbf{G}^{-1})
\end{aligned}
\end{equation}

than the multiplicative output rational weight can be chosen so that it covers the set:
\begin{equation}
\begin{aligned}
|w_O(jw)|\geq \l_O(w), \text{\quad} \forall w
\end{aligned}
\end{equation}

To derive $w_O(jw)$ we try a simple first-order weight that matches this limiting behaviour:
\begin{equation}
\begin{aligned}
	w_O(jw)=\frac{0.2081 s + 0.009141}{s + 0.01791}
\end{aligned}
\label{eq:wo}
\end{equation}
This corresponds to 20.81\% low frequency and 51,04\% high frequency relative uncertainty. \newline \newline
As seen from the red line in Fig.~\ref{fig:wo_uncert_min} this weight gives a good fit of $l_O(w)$.

\begin{figure}[!h]
	\includegraphics[width=0.45\textwidth,center]{images/wo_min_phase}
	\caption{RGA values over frequency for min and non-min phase system}
	\label{fig:wo_uncert_min}
\end{figure}

For the non min-phase system, similar as above, we  get the first order weight
\begin{equation}
\begin{aligned}
	w_{Onon}(jw)= \frac{0.205 s + 0.005729}{s + 0.01286}
\end{aligned}
\label{eq:wonon}
\end{equation}

It is shown once more in the Fig.~\ref{fig:wo_uncert_non-min} that it satisfies our expectations
\begin{figure}[!h]
	\includegraphics[width=0.45\textwidth,center]{images/wo_non-min_phase}
	\caption{RGA values over frequency for min and non-min phase system}
	\label{fig:wo_uncert_non-min}
\end{figure}

\section{Robust stability with dynamic uncertainty}\label{robust_dynamic}
Theoretically, the robust stability of a MIMO system is determined by firstly calculating the $(M \Delta)$ system by computing $N$ as LTF of the general plant $P$ and $K$ and taking the $N_{11}$. This corresponds to \[\textbf{RS}<=>||w_O\mathbf{GK}(\mathbf{I}+\mathbf{GK})^{-1}||_{\infty}=||w_O\mathbf{T}||_{\infty}<1\] for plants perturbed with multiplicative output uncertainty. In practice, using 'robstab' in Matlab gives an idea on how robust our systems are. For the dynamic uncertainties we get a tolerance of up to 217\% of modeled uncertainty for the min phase system. On the other hand the non-min phase system can allow perturbations up to 130\% of the modeled uncertainty.

\section{Robust stability with parametric uncertainty}
In contrast to Quest.~\ref{robust_dynamic} the robust stability values for the plant with parametric uncertainty are 6.14 and 1.83 which means that the plants can tolerate up to 614\% and 183\% of the modeled uncertainty respectively.
We can see that one can allow more perturbations if parametric uncertainty is used. This shows that the dynamic uncertainty description is conservative in general and one may even assume that the system gets unstable even though it does not.
