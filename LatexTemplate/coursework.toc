\contentsline {chapter}{\nonumberline \hskip 1em\relax \ Preface}{3}% 
\contentsline {chapter}{\numberline {1}System analysis}{4}% 
\contentsline {section}{\numberline {1.1}Trimming the model}{4}% 
\contentsline {section}{\numberline {1.2}System representation and analysis}{4}% 
\contentsline {subsubsection}{Poles}{4}% 
\contentsline {subsubsection}{Zeros}{4}% 
\contentsline {subsubsection}{Pole and zero-directions}{4}% 
\contentsline {subsubsection}{Minimum phase system}{5}% 
\contentsline {subsubsection}{Non-minimum phase system}{5}% 
\contentsline {section}{\numberline {1.3}Relative Gain Array (RGA)}{6}% 
\contentsline {section}{\numberline {1.4}Parametric uncertainty for nonlinearities}{7}% 
\contentsline {section}{\numberline {1.5}Singular values}{7}% 
\contentsline {chapter}{\numberline {2}Decentralized control and uncertainty modeling}{8}% 
\contentsline {section}{\numberline {2.1}Decentralized controller based on linear and nonlinear models}{8}% 
\contentsline {section}{\numberline {2.2}Model uncertainty and weighting function}{12}% 
\contentsline {section}{\numberline {2.3}Robust stability with dynamic uncertainty}{13}% 
\contentsline {section}{\numberline {2.4}Robust stability with parametric uncertainty}{13}% 
\contentsline {chapter}{\numberline {3}Robust control design using mixed sensitivity approach}{14}% 
\contentsline {section}{\numberline {3.1}$H_{\infty }$ loop-shaping}{14}% 
\contentsline {section}{\numberline {3.2}$H_{\infty }$ mixed sensitivity design prerequisites }{15}% 
\contentsline {section}{\numberline {3.3}Nonlinear model and a $H_{\infty }$ controller}{18}% 
\contentsline {section}{\numberline {3.4}Robust performance}{19}% 
\contentsline {subsubsection}{Nominal Stability}{19}% 
\contentsline {subsubsection}{Robust Stability}{20}% 
\contentsline {subsubsection}{Nominal Performance}{20}% 
\contentsline {subsubsection}{Robust Performance}{21}% 
\contentsline {chapter}{\numberline {4}Loop/Plant modification}{22}% 
\contentsline {section}{\numberline {4.1}Nonlinear model for leakage}{22}% 
\contentsline {section}{\numberline {4.2}Linearization for leakage}{22}% 
\contentsline {section}{\numberline {4.3}Controller design for leakage}{23}% 
\contentsline {section}{\numberline {4.4}Evaluation of disturbance rejection for leakage}{24}% 
\contentsline {section}{\numberline {4.5}Evaluation of controller based on leakage}{25}% 
