init_all
%% Task 1

%% 1. Trimm the model 
% done in init_all: choosing min_phase=1 builds the minimum phase system
% otherwise the non-minimium phase system is built.

%% 2. System representation and analysis
% Find tf
C_new = [1 0 0 0; 0 1 0 0];
D_new = zeros(2,2);

%% min phase system
G = tf(ss(A,B,C_new,D_new));
% Find poles and zeros 
[V,Diag,W] = eig(A);
poles = diag(Diag)
tzeros = tzero(A,B,C_new,D_new)
% Direction of poles
input_pol_directions = B'*W; % ith column is the direction of poles(i)
output_pol_directions = C_new*V;
for i=1:length(input_pol_directions) % normalize so that directions have length 1
input_pol_directions(:,i)=input_pol_directions(:,i)/norm(input_pol_directions(:,i));
output_pol_directions(:,i)=output_pol_directions(:,i)/norm(output_pol_directions(:,i));
end

% Direction of zeros
input_zero_directions = zeros(2,1);
output_zero_directions = zeros(2,1);
for index = 1:length(tzeros)
    [OU,SINGULAR,IN] = svd(evalfr(G,tzeros(index)));
    input_zero_directions(:,index)=IN(:,end);
    output_zero_directions(:,index) = OU(:,end);
end

%% non-min phase system
G_non = tf(ss(A_non,B_non,C_non,D_non)); % matrices are built in init_all
% Find poles and zeros 
[V_non,Diag_non,W_non] = eig(A_non);
poles_non = diag(Diag)
tzeros_non = tzero(A_non,B_non,C_new,D_new)
% Direction of poles
input_pol_directions_non = B_non'*W_non; % ith column is the direction of poles(i)
output_pol_directions_non = C_new*V_non;
for i=1:length(input_pol_directions_non) % normalize so that directions have length 1
input_pol_directions_non(:,i)=input_pol_directions_non(:,i)/norm(input_pol_directions_non(:,i));
output_pol_directions_non(:,i)=output_pol_directions_non(:,i)/norm(output_pol_directions_non(:,i));
end

% Direction of zeros
input_zero_directions_non= zeros(2,1);
output_zero_directions_non = zeros(2,1);
for index = 1:length(tzeros)
    [OU,SINGULAR,IN] = svd(evalfr(G,tzeros(index)));
    input_zero_directions_non(:,index)=IN(:,end);
    output_zero_directions_non(:,index) = OU(:,end);
end



%% 3. Relative Gain Array (RGA)
% static RGA
G0 = evalfr(G,0);
RGA = G0.*inv(G0)'
pairing = eye(2);

G0_non = evalfr(G_non,0);
RGA_non = G0_non.*inv(G0_non)'
pairing_non = [0 1; 1 0];

%dynamic RGA
omega = logspace(-5,2,61);
for i = 1:length(omega)
Gf = freqresp(G,omega(i)); % G(j?)
Gf_non = freqresp(G_non,omega(i)); % G(j?)

RGAw(:,:,i) = Gf.*inv(Gf).'; % RGA at frequency omega
RGAw_non(:,:,i) = Gf_non.*inv(Gf_non).'; % RGA at frequency omega
RGAno(i) = sum(sum(abs(RGAw(:,:,i) - eye(2)))); % RGA number
RGAno_non(i) = sum(sum(abs(RGAw_non(:,:,i) - eye(2)))); % RGA number

end
lamda11=abs(RGAw(2,2,:));
lamda11=reshape(lamda11,1,61);
lamda12=abs(RGAw(2,1,:));
lamda12=reshape(lamda12,1,61);
subplot(211)
semilogx(omega,lamda11,'r')
hold on
semilogx(omega,lamda12,'b')
ylabel('\lambda [-]') 
xlabel('f [rad/s]') 
text(1.96e-2,1.27,'|\lambda_{11}|=|\lambda_{22}|', 'Color','red')
text(1e-2,0.45,'|\lambda_{12}|=|\lambda_{21}|','Color','blue')
title('Dynamic RGA for G_{min}')
hold off
xlim([1e-4 1])
lamda11_non=abs(RGAw_non(2,2,:));
lamda11_non=reshape(lamda11_non,1,61);
lamda12_non=abs(RGAw_non(2,1,:));
lamda12_non=reshape(lamda12_non,1,61);
subplot(212)
semilogx(omega,lamda11_non,'r')
hold on
semilogx(omega,lamda12_non,'b')
ylabel('\lambda [-]') 
xlabel('f [rad/s]')
text(1e-2,1.4,'|\lambda_{12}|=|\lambda_{21}|', 'Color','blue')
text(1e-3,0.82,'|\lambda_{11}|=|\lambda_{22}|','Color','red')
title('Dynamic RGA for G_{non-min}')
hold off
xlim([1e-4 1])

%% 4. Parametric Uncertainty for nonlinearities

%% 5. Singular values
figure(3)
subplot(121), sigma ( Gss, 'b' ), ylim([-25 20]), xlim([1e-4 2]), xticks([1e-4 1e-3 1e-2 1e-1 1e0]), title('Singular Values for G_{min}'), grid
subplot(122),sigma ( Gss_non,'r' ), ylim([-25 20]), xlim([1e-4 2]), xticks([1e-4 1e-3 1e-2 1e-1 1e0]), title('Singular Values for G_{non-min}'), grid

% system at both operating points is ill conditioned at steady state