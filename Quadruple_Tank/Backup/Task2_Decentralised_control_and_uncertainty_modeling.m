init_all
%% Task 2
% decentralised controller in general
K1 = 0; T1 = 0;
K2 = 0; T2 = 0;
s=tf('s');
K = [K1*(1+1/(T1*s)) 0;0 K2*(1+1/(T2*s))];

K1_non = 89.746; T1_non = 63.29113924;
K2_non = 20.1348; T2_non = 91.7431;
K_non = [0 K1_non*(1+1/(T1_non*s));K2_non*(1+1/(T2_non*s)) 0];

% Niederlinski Index(NI) min phase system
G0_pair = G0.*pairing;
G0_pair=G0_pair+(G0_pair==0);
NI = det(G0)/prod(G0_pair,'all');
if NI > 0
    fprintf('Since NI is positive the minimum phase system is stabile for the choosen pairings.\n');
else
    fprintf('Since NI is smaller than zero the minimum phase system is instabile for the choosen pairings.\n');
end

% Niederlinski Index(NI) non-min phase system
G0_pair_non = G0_non.*pairing_non;
G0_pair_non=G0_pair_non+(G0_pair_non==0);
NI_non = det(G0)/prod(G0_pair_non,'all');
if NI_non > 0
    fprintf('Since NI is positive the non-minimum phase system is stabile for the choosen pairings.\n');
else
   fprintf('Since NI is smaller than zero the non-minimum phase system is instabile for the choosen pairings.\n');
end

%% Question 1
%% min phase decentralised controller
g11 = G(1,1);
K11=10*(1+1/(10*s));
figure(1)
subplot(1,2,1)
step(feedback(g11*K11,1))
stepinfo(feedback(g11*K11,1))

g22 = G(2,2);
K22=10*(1+1/(10*s));
subplot(1,2,2)
step(feedback(g22*K22,1))
stepinfo(feedback(g22*K22,1))

K=[K11 0;0 K22];
X(end:6)=0;
Kss=minreal(ss(K));

t=0:0.1:400; t=t';
u=ones(4001,1);
u(1:5)=0;
[y_1,t,x]=lsim(feedback(Gss*Kss,eye(2)),[u,u],t,[0; 0; 0; 0; 0; 0]);
% initial(feedback(Gss*Kss,eye(2)),X); hold on


%% non min phase decentralised controller
g12_non = G_non(1,2);
K1_non = 0.39   ; T1_non = 1/0.008;
K12_non =K1_non*(1+1/(T1_non*s));
figure(2)
subplot(1,2,1)
step(feedback(g12_non*K12_non,1),'r')
% stepinfo(feedback(g12_non*K12_non,1))

g21_non = G_non(2,1);
K2_non = 0.65; T2_non = 1/0.005;
K21_non =K2_non*(1+1/(T2_non*s)); 
subplot(1,2,2)
step(feedback(g21_non*K21_non,1),'r')
% stepinfo(feedback(g21_non*K21_non,1))

K_non = [0 K12_non; K21_non 0 ];
Kss_non=minreal(ss(K_non));
X_non(end:6)=0;

hold on
[y_2,t2,x]=lsim(feedback(Gss_non*Kss_non,eye(2)),[u,u],t,[0; 0; 0; 0; 0; 0],'r');
% initial(feedback(Gss_non*Kss_non,eye(2)),X_non);
%% plot
figure(3)
plot(t,u,'b',t,y_1,'b',t,y_2,'r')


%% Question 2
a1 = ureal ( 'a1', a1, 'Range', [0.95*a1, 1.05*a1] );
a2 = ureal ( 'a2', a2, 'Range', [0.95*a2, 1.05*a2] );
a3 = ureal ( 'a3', a3, 'Range', [0.95*a3, 1.05*a3] );
a4 = ureal ( 'a4', a4, 'Range', [0.95*a4, 1.05*a4] );

% Parameters of min phase operating point 
g1 = ureal ( 'g1', g1, 'Range', [0.9*g1, 1.1*g1] );
g2 = ureal ( 'g2', g2, 'Range', [0.9*g2, 1.1*g2] );
k1 = ureal ( 'k1', k1, 'Range', [0.9*k1, 1.1*k1] );
k2 = ureal ( 'k2', k2, 'Range', [0.9*k2, 1.1*k2] );

% Parameters of non-min phase operating point
g1_non = ureal ( 'g1_non', g1_non, 'Range', [0.9*g1_non, 1.1*g1_non] );
g2_non = ureal ( 'g2_non', g2_non, 'Range', [0.9*g2_non, 1.1*g2_non] );
k1_non = ureal ( 'k1_non', k1_non, 'Range', [0.9*k1_non, 1.1*k1_non] );
k2_non = ureal ( 'k2_non', k2_non, 'Range', [0.9*k2_non, 1.1*k2_non] );


%% Min phase state space matrixes
    a11=-a1/A1*sqrt(g/2/h10);
    a13=a3/A1*sqrt(g/2/h30);
    a22=-a2/A2*sqrt(g/2/h20);
    a24=a4/A2*sqrt(g/2/h40);
    a33=-a3/A3*sqrt(g/2/h30);
    a44=-a4/A4*sqrt(g/2/h40);
    Ap=[a11 0 a13 0;0 a22 0 a24;0 0 a33 0;0 0 0 a44];

    b11=g1*k1/A1;
    b22=g2*k2/A2;
    b32=(1-g2)*k2/A3;
    b41=(1-g1)*k1/A4;
    Bp=[b11 0;0 b22;0 b32;b41 0];

    Gssp=uss(Ap,Bp,C_non,D_non); % plant with parametric uncertainty
    %%
% Approximate multiplicative output uncertainty of min phase system
opts = wcOptions('VaryFrequency','on');
[~,~,info1] = wcgain(Gss^-1*(Gssp-Gss),{0.01,50},opts); % worst singular value over the freqences
lo=frd(info1.Bounds(:,1),info1.Frequency);
constraint.LowerBound = lo;
constraint.UpperBound = [];
wo = fitmagfrd( lo , 2, [], [], constraint ); % fit a function wo of third grade with a lower bound lo
delta=ultidyn('H',[2 2],'Bound',1); 
% check that the approximation worked
sigma(Gss^-1*(Gssp-Gss))
hold on
sigma(wo,'-*r')
Gssdyn=Gss*(eye(2)+wo*delta); % plant with dynamic uncertainty

%% Non-min phase system space matrixes
    a11_non=-a1/A1*sqrt(g/2/h10_non);
    a13_non=a3/A1*sqrt(g/2/h30_non);
    a22_non=-a2/A2*sqrt(g/2/h20_non);
    a24_non=a4/A2*sqrt(g/2/h40_non);
    a33_non=-a3/A3*sqrt(g/2/h30_non);
    a44_non=-a4/A4*sqrt(g/2/h40_non);
    Ap_non=[a11_non 0 a13_non 0;0 a22_non 0 a24_non;0 0 a33_non 0;0 0 0 a44_non];

    b11_non=g1_non*k1_non/A1;
    b22_non=g2_non*k2_non/A2;
    b32_non=(1-g2_non)*k2_non/A3;
    b41_non=(1-g1_non)*k1_non/A4;
    Bp_non=[b11_non 0;0 b22_non;0 b32_non;b41_non 0];
    
    Gssp_non=uss(Ap_non,Bp_non,C_non,D_non);
     %%
% Approximate additive uncertainty of non-min phase system
opts = wcOptions('VaryFrequency','on');
[~,~,info1_non] = wcgain(Gssp_non-Gss_non,{0.01,50},opts); % choosing multiplicative uncertainty the function didn't work.
la_non=frd(info1_non.Bounds(:,1),info1_non.Frequency);
constraint_non.LowerBound = la_non;
constraint_non.UpperBound = [];
wa_non = fitmagfrd( la_non , 2, [], [], constraint_non );
delta=ultidyn('H',[2 2],'Bound',1); 
% check that the approximation worked
sigma(Gssp_non-Gss_non)
hold on
sigma(wa_non,'-r')

Gssdyn_non=Gss_non*(eye(2)+wa_non*delta); % plant with dynamic uncertainty

%% Question 3  Robust stability with dynamic uncertainty
[ stabMarg, destabUnc, report ]= robuststab(feedback(Gssdyn*K,eye(2)))
[ stabMarg_non, destabUnc_non, report_non ]= robuststab(feedback(Gssdyn_non*K_non,eye(2)))

%% Question 4  Robust stability with parametric uncertainty
[ stabMarg_p, destabUnc_p, report_p ]= robuststab(feedback(Gssp*K,eye(2)))
[ stabMarg_p_non, destabUnc_p_non, report_p_non ]= robuststab(feedback(Gssp_non*K_non,eye(2)))