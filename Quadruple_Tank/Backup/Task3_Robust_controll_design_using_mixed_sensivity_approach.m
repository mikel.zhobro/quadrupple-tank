init_all
Task2_Question2
%% Prerequisits

[n,m]=size(B);
p=size(C_non,1);
disp( rank( ctrb( Gss_non ) ) == min( size( ctrb( Gss_non ) ) ) )
disp( rank( obsv( Gss_non ) ) == min( size( obsv( Gss_non ) ) ) )
% disp( rank( D12 ) == min( size( D12 ) ) )
% disp( rank( D21 ) == min( size( D21 ) ) )
% disp( D11 == 0 )
% disp( D22 == 0 )


display (n==rank(obsv(Gss)))
display (n==rank(ctrb(Gss)))
%% Question 1 H-inf loop-shaping
s= tf('s');
% non min phase system's weights
W1=0.8*(s+10)/(s+0.01);
W2=0.1*(s+0.005)/(s/7+0.1);  
% non min phase system's weights
W1_non=0.8*(s+1)/(s+0.001);
W2_non=0.6*(s+0.001)/(s/100+0.1); 
W2_non=0.8*(s+0.005)/(s/7+0.1); 
%% plot
bodemag(W1,W1_non)
legend
figure
bodemag(W2,W2_non)
legend
hold off
%% Question 2 H-inf mixed sensivity prerequisites

% % min phase system
% P11=[W1;zeros(2)]; P12=[-W1*G; W2];
% P21=eye(2); P22=-G;
% P=[P11 P12; P21 P22];
% [K,CL,gamma]=hinfsyn(P,2,2);% CL = lft(P,K); gamma=hinfnorm(CL);
% gamma
% initial(CL,ones(40,1));
% %title({'$\tilde(e)_1$','$\tilde(e)_22$','$\tilde(u)_22$','$\tilde(u)_22$'},'interpreter', 'latex')
% 
% % non-min phase system
% P11_non=[W1;zeros(2)]; P12_non=[-W1*G_non; W2];
% P21_non=eye(2); P22_non=-G_non;2
% P_non=[P11_non P12_non; P21_non P22_non];
% [K_non,CL_non,gamma_non]=hinfsyn(P_non,2,2);
% gamma_non

%% Question 2 H-inf mixed sensivity prerequisites
%% min phase system
[K_inf,CL,GAM]=mixsyn(G,W1,[],W2)
S = feedback(eye(2),G*K_inf);
R = K_inf*S;
T = eye(2)-S;
%% plot
sigma(S,'b',T,'r',GAM/W1,'b-.',GAM/ss(W2),'r-.')
grid on
legend('S','T','GAM/W1','GAM/W2','Location','SouthWest')
%% time response
t=0:0.1:100; t=t';
u=ones(1001,1);
u(1:100)=0;
figure(1)
subplot(1,2,1)
lsim(feedback(G*K_inf,eye(2)),13*[u,u],t,zeros(16,1));
title('Min phase system with H_{inf} controller')
%% non-min phase system
[K_inf_non,CL_non,GAM_non]=mixsyn(G_non,W1_non,W2_non,[]);
S_non = feedback(eye(2),G_non*K_inf_non);
R_non = K_inf_non*S_non;
T_non = eye(2)-S_non;
%% plot
sigma(S_non,'b',R_non,'r',GAM_non/W1_non,'b-.',GAM_non/ss(W2_non),'r-.')%,GAM/ss(W3_non),'g-.')
grid on
legend('S_non','R_non','GAM/W1_non','GAM/W2_non','GAM/W2_non','Location','SouthWest')
%% time response
t=0:0.1:100; t=t';
u=ones(1001,1);
u(1:100)=0;
% figure(2)
hold on
subplot(1,2,2)
lsim(feedback(G_non*K_inf_non,eye(2)),13*[u,u],t,zeros(16,1),'r');
title('Non-min phase system with H_{inf} controller')

%% Question 3 Nonlinear model and a H-inf controller
%% Question 4 Robust performance
% robust stability
hinfnorm(W1*S)<1
hinfnorm(W1_non*S_non)<1
% nominal performance
hinfnorm(W2*T)<1
hinfnorm(W2_non*R_non)<1
%robust performance of min system
Sp = feedback(eye(2),Gssp*K_inf); %uncertainty has to be activated from task 2
robgain(Sp,1.2)

%% decentralized
Sdec = feedback(eye(2),G*K);
Tdec = eye(2)-S;
S_nondec = feedback(eye(2),G_non*K_non);
R_nondec = K_non*S_non;

%% robust stability
hinfnorm(W1*Sdec)
hinfnorm(W1_non*S_nondec)
% nominal performance
hinfnorm(W2*Tdec)
hinfnorm(W2_non*R_nondec)

