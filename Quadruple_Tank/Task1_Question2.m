%% 2. System representation and analysis
% Find tf
C_new = [1 0 0 0; 0 1 0 0];
D_new = zeros(2,2);

%% min phase system
G = tf(ss(A,B,C_new,D_new));
% Find poles and zeros 
[V,Diag,W] = eig(A);
poles = diag(Diag)
tzeros = tzero(A,B,C_new,D_new)
% Direction of poles
input_pol_directions = B'*W; % ith column is the direction of poles(i)
output_pol_directions = C_new*V;
for i=1:length(input_pol_directions) % normalize so that directions have length 1
input_pol_directions(:,i)=input_pol_directions(:,i)/norm(input_pol_directions(:,i));
output_pol_directions(:,i)=output_pol_directions(:,i)/norm(output_pol_directions(:,i));
end

% Direction of zeros
input_zero_directions = zeros(2,1);
output_zero_directions = zeros(2,1);
for index = 1:length(tzeros)
    [OU,SINGULAR,IN] = svd(evalfr(G,tzeros(index)));
    input_zero_directions(:,index)=IN(:,end);
    output_zero_directions(:,index) = OU(:,end);
end

%% non-min phase system
G_non = tf(ss(A_non,B_non,C_non,D_non)); % matrices are built in init_all
% Find poles and zeros 
[V_non,Diag_non,W_non] = eig(A_non);
poles_non = diag(Diag)
tzeros_non = tzero(A_non,B_non,C_new,D_new)
% Direction of poles
input_pol_directions_non = B_non'*W_non; % ith column is the direction of poles(i)
output_pol_directions_non = C_new*V_non;
for i=1:length(input_pol_directions_non) % normalize so that directions have length 1
input_pol_directions_non(:,i)=input_pol_directions_non(:,i)/norm(input_pol_directions_non(:,i));
output_pol_directions_non(:,i)=output_pol_directions_non(:,i)/norm(output_pol_directions_non(:,i));
end

% Direction of zeros
input_zero_directions_non= zeros(2,1);
output_zero_directions_non = zeros(2,1);
for index = 1:length(tzeros)
    [OU,SINGULAR,IN] = svd(evalfr(G,tzeros(index)));
    input_zero_directions_non(:,index)=IN(:,end);
    output_zero_directions_non(:,index) = OU(:,end);
end