%% 3. Relative Gain Array (RGA)
% static RGA
G0 = evalfr(G,0);
RGA = G0.*inv(G0)'
pairing = eye(2);

G0_non = evalfr(G_non,0);
RGA_non = G0_non.*inv(G0_non)'
pairing_non = [0 1; 1 0];

%dynamic RGA
omega = logspace(-5,2,61);
for i = 1:length(omega)
Gf = freqresp(G,omega(i)); % G(j?)
Gf_non = freqresp(G_non,omega(i)); % G(j?)

RGAw(:,:,i) = Gf.*inv(Gf).'; % RGA at frequency omega
RGAw_non(:,:,i) = Gf_non.*inv(Gf_non).'; % RGA at frequency omega
RGAno(i) = sum(sum(abs(RGAw(:,:,i) - eye(2)))); % RGA number
RGAno_non(i) = sum(sum(abs(RGAw_non(:,:,i) - eye(2)))); % RGA number

end
lamda11=abs(RGAw(2,2,:));
lamda11=reshape(lamda11,1,61);
lamda12=abs(RGAw(2,1,:));
lamda12=reshape(lamda12,1,61);
subplot(211)
semilogx(omega,lamda11,'r')
hold on
semilogx(omega,lamda12,'b')
ylabel('\lambda [-]') 
xlabel('f [rad/s]') 
text(1.96e-2,1.27,'|\lambda_{11}|=|\lambda_{22}|', 'Color','red')
text(1e-2,0.45,'|\lambda_{12}|=|\lambda_{21}|','Color','blue')
title('Dynamic RGA for G_{min}')
hold off
xlim([1e-4 1])
lamda11_non=abs(RGAw_non(2,2,:));
lamda11_non=reshape(lamda11_non,1,61);
lamda12_non=abs(RGAw_non(2,1,:));
lamda12_non=reshape(lamda12_non,1,61);
subplot(212)
semilogx(omega,lamda11_non,'r')
hold on
semilogx(omega,lamda12_non,'b')
ylabel('\lambda [-]') 
xlabel('f [rad/s]')
text(1e-2,1.4,'|\lambda_{12}|=|\lambda_{21}|', 'Color','blue')
text(1e-3,0.82,'|\lambda_{11}|=|\lambda_{22}|','Color','red')
title('Dynamic RGA for G_{non-min}')
hold off
xlim([1e-4 1])