%% 5. Singular values
figure(3)
subplot(121), sigma ( Gss, 'b' ), ylim([-25 20]), xlim([1e-4 2]), xticks([1e-4 1e-3 1e-2 1e-1 1e0]), title('Singular Values for G_{min}'), grid
subplot(122),sigma ( Gss_non,'r' ), ylim([-25 20]), xlim([1e-4 2]), xticks([1e-4 1e-3 1e-2 1e-1 1e0]), title('Singular Values for G_{non-min}'), grid

% system at both operating points is ill conditioned at steady state