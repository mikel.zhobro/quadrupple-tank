%% 
%% Question 1
%% a)min phase decentralised controller
g11 = G(1,1);
K11=10*(1+1/(10*s));
g22 = G(2,2);
K22=10*(1+1/(10*s));

K=[K11 0;0 K22];
Kss=minreal(ss(K));
    %% step responses of paired subsystems
    figure(1)
    subplot(1,2,1)
    step(feedback(g11*K11,1))
    %stepinfo(feedback(g11*K11,1))
    subplot(1,2,2)
    step(feedback(g22*K22,1))
    %stepinfo(feedback(g22*K22,1))

%% b)non min phase decentralised controller
g12_non = G_non(1,2);
K1_non = 0.39   ; T1_non = 1/0.008;
K12_non =K1_non*(1+1/(T1_non*s));

g21_non = G_non(2,1);
K2_non = 0.65; T2_non = 1/0.005;
K21_non =K2_non*(1+1/(T2_non*s)); 

K_non = [0 K12_non; K21_non 0 ];
Kss_non=minreal(ss(K_non));

    %% step responses of paired subsystems
    figure(2)
    subplot(1,2,1)
    step(feedback(g12_non*K12_non,1),'r')
    % stepinfo(feedback(g12_non*K12_non,1))
    subplot(1,2,2)
    step(feedback(g21_non*K21_non,1),'r')
    % stepinfo(feedback(g21_non*K21_non,1))

    

%% time responses of systems with decentralized controllers with h_0 and h_non_0 initial states
%% minphase
u=ones(501,1);
u(1:2)=0;
t=0:0.1:50; t=t';
[y_1,t,x]=lsim(feedback(Gss*Kss,eye(2)),[u,u],t,[0; 0; 0; 0; 0; 0]);
subplot(2,1,1)
plot(t,y_1(:,1))
hold on
subplot(2,1,2)
plot(t,y_1(:,2))
hold on
%% simulink
% subplot(2,1,1)
% plot(h1_min_diag.Time,h1_min_diag.Data(:,1))
% subplot(2,1,2)
% plot(h2_min_diag.Time,h2_min_diag.Data(:,1))

%% nonmin phase
u=ones(15001,1);
u(1:5)=0;
t=0:0.1:1500; t=t';
[y_2,t2,x]=lsim(feedback(Gss_non*Kss_non,eye(2)),[u,u],t,[0; 0; 0; 0; 0; 0],'r');

subplot(2,1,1)
plot(t,y_2(:,1))
hold on
subplot(2,1,2)
plot(t,y_2(:,2))
hold on
    %% simulink
%     subplot(2,1,1)
%     plot(h1_non_min_diag.Time,h1_non_min_diag.Data(:,1))
%     subplot(2,1,2)
%     plot(h2_non_min_diag.Time,h2_non_min_diag.Data(:,1))


%% plot with h_0 and h_non_0 initial states

% % % [y_1,t,x]=lsim(feedback(Gss*Kss,eye(2)),6*[u,u],t,[X;0; 0]);
% % % [y_2,t2,x]=lsim(feedback(Gss_non*Kss_non,eye(2)),6*[u,u],t,[X_non; 0; 0],'r');
% % % figure
% % % plot(t,6*u,'k',t,y_1,'b',t,y_2,'r')%% a)min phase decentralised controller
