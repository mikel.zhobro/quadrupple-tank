%%
%% Question 2
a1_p =a1; a1_p= ureal ( 'a1', a1_p, 'Range', [0.95*a1_p, 1.05*a1_p] );
a2_p =a2; a2_p = ureal ( 'a2', a2_p, 'Range', [0.95*a2_p, 1.05*a2_p] );
a3_p =a3; a3_p = ureal ( 'a3', a3_p, 'Range', [0.95*a3_p, 1.05*a3_p] );
a4_p =a4; a4_p = ureal ( 'a4', a4_p, 'Range', [0.95*a4_p, 1.05*a4_p] );

% Parameters of min phase operating point 
g1_p=g1; g1_p = ureal ( 'g1', g1_p, 'Range', [0.9*g1_p, 1.1*g1_p] );
g2_p=g2; g2_p = ureal ( 'g2', g2_p, 'Range', [0.9*g2_p, 1.1*g2_p] );
k1_p=k1; k1_p = ureal ( 'k1', k1_p, 'Range', [0.9*k1_p, 1.1*k1_p] );
k2_p=k2; k2_p = ureal ( 'k2', k2_p, 'Range', [0.9*k2_p, 1.1*k2_p] );

% Parameters of non-min phase operating point
g1_non_p=g1_non; g1_non_p = ureal ( 'g1_non', g1_non_p, 'Range', [0.9*g1_non_p, 1.1*g1_non_p] );
g2_non_p=g2_non; g2_non_p = ureal ( 'g2_non', g2_non_p, 'Range', [0.9*g2_non_p, 1.1*g2_non_p] );
k1_non_p=k1_non; k1_non_p = ureal ( 'k1_non', k1_non_p, 'Range', [0.9*k1_non_p, 1.1*k1_non_p] );
k2_non_p=k2_non; k2_non_p = ureal ( 'k2_non', k2_non_p, 'Range', [0.9*k2_non_p, 1.1*k2_non_p] );


%% a)Min phase state space matrixes
a11=-a1_p/A1*sqrt(g/2/h10);
a13=a3_p/A1*sqrt(g/2/h30);
a22=-a2_p/A2*sqrt(g/2/h20);
a24=a4_p/A2*sqrt(g/2/h40);
a33=-a3_p/A3*sqrt(g/2/h30);
a44=-a4_p/A4*sqrt(g/2/h40);
Ap=[a11 0 a13 0;0 a22 0 a24;0 0 a33 0;0 0 0 a44];

b11=g1_p*k1_p/A1;
b22=g2_p*k2_p/A2;
b32=(1-g2_p)*k2_p/A3;
b41=(1-g1_p)*k1_p/A4;
Bp=[b11 0;0 b22;0 b32;b41 0];

Gssp=uss(Ap,Bp,C_non,D_non); % plant with parametric uncertainty
    %% -Approximate multiplicative output uncertainty of min phase system
    Task2_Question2_min_phase_output_mult_uncertainty
    %% -plant with dynamic uncertainty
    delta=ultidyn('delta',[2 2],'Bound',1); 
    Gssdyn=Gss*(eye(2)+wo*delta); % plant with dynamic uncertainty
    
    
%% b)Non-min phase system space matrixes
a11_non=-a1_p/A1*sqrt(g/2/h10_non);
a13_non=a3_p/A1*sqrt(g/2/h30_non);
a22_non=-a2_p/A2*sqrt(g/2/h20_non);
a24_non=a4_p/A2*sqrt(g/2/h40_non);
a33_non=-a3_p/A3*sqrt(g/2/h30_non);
a44_non=-a4_p/A4*sqrt(g/2/h40_non);
Ap_non=[a11_non 0 a13_non 0;0 a22_non 0 a24_non;0 0 a33_non 0;0 0 0 a44_non];

b11_non=g1_non_p*k1_non_p/A1;
b22_non=g2_non_p*k2_non_p/A2;
b32_non=(1-g2_non_p)*k2_non_p/A3;
b41_non=(1-g1_non_p)*k1_non_p/A4;
Bp_non=[b11_non 0;0 b22_non;0 b32_non;b41_non 0];
    
Gssp_non=uss(Ap_non,Bp_non,C_non,D_non);
    %% -Approximate multiplicative uncertainty of non-min phase system
    Task2_Question2_non_min_phase_output_mult_uncertainty
    %% -plant with dynamic uncertainty
    Gssdyn_non=(eye(2)+wo_non*delta)*Gss_non; % plant with dynamic uncertainty