%% -Approximate multiplicative output uncertainty of min phase system
SIG=zeros(100,1); % worst singular value over the freqences over all 50 samples
% hold on
for i=1:50
[sigmat,w1]=sigma(usample(((Gssp_non-Gss_non)*Gss_non^-1),1),logspace(-2,3,100));    
% sigma(frd(sigmat(1,:),logspace(-2,3,100)))
SIG(:,i)=sigmat(1,:)';
end

lo_non=frd(max(SIG'),logspace(-2,3,100));

constraint.LowerBound = lo_non;
constraint.UpperBound = [];
wo_non = fitmagfrd( lo_non , 1, [], [], constraint ); %fit a function wo of second grade with a lower bound lo_non

%% -check that the approximation worked
% uncomment row 3 6 and 18 for the plot
% sigma(wo_non,logspace(-2,3,100),'-*r')