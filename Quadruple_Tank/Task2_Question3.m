%%
%% Question 3  Robust stability with dynamic uncertainty
[ stabMarg, destabUnc, report ]= robuststab(feedback(Gssdyn*K,eye(2)));stabMarg
[ stabMarg_non, destabUnc_non, report_non ]= robuststab(feedback(Gssdyn_non*K_non,eye(2))); stabMarg_non

%% or
if hinfnorm(wo*feedback(eye(2),Gssdyn*K)) <1
    fprintf('Min-phase plant with dynamic disturbance is robust stable!\n')
else
    fprintf('Min-phase plant with dynamic disturbance is NOT robust stable!\n')
end

if hinfnorm(wo_non*feedback(eye(2),Gssdyn_non*K_non)) <1
    fprintf('Non_min-phase plant with dynamic disturbance is robust stable!\n')
else
    fprintf('Non_min-phase plant with dynamic disturbance is NOT robust stable!\n')

end