%% Question 4  Robust stability with parametric uncertainty
[ stabMarg_p, destabUnc_p, report_p ]= robuststab(feedback(Gssp*K,eye(2))); stabMarg_p
[ stabMarg_p_non, destabUnc_p_non, report_p_non ]= robuststab(feedback(Gssp_non*K_non,eye(2))); stabMarg_p_non