init_all
G0 = evalfr(G,0);
G0_non = evalfr(G_non,0);
pairing = eye(2);
pairing_non = [0 1; 1 0];
%% Task 2
% decentralised controller in general
K1 = 0; T1 = 0;
K2 = 0; T2 = 0;
s=tf('s');
K = [K1*(1+1/(T1*s)) 0;0 K2*(1+1/(T2*s))];

K1_non = 89.746; T1_non = 63.29113924;
K2_non = 20.1348; T2_non = 91.7431;
K_non = [0 K1_non*(1+1/(T1_non*s));K2_non*(1+1/(T2_non*s)) 0];

% Niederlinski Index(NI) min phase system
G0_pair = G0.*pairing;
G0_pair=G0_pair+(G0_pair==0);
NI = det(G0)/prod(G0_pair,'all');
if NI > 0
    fprintf('Since NI is positive the minimum phase system is stabile for the choosen pairings.\n');
else
    fprintf('Since NI is smaller than zero the minimum phase system is instabile for the choosen pairings.\n');
end

% Niederlinski Index(NI) non-min phase system
G0_pair_non = G0_non.*pairing_non;
G0_pair_non=G0_pair_non+(G0_pair_non==0);
NI_non = det(G0)/prod(G0_pair_non,'all');
if NI_non > 0
    fprintf('Since NI is positive the non-minimum phase system is stabile for the choosen pairings.\n');
else
   fprintf('Since NI is smaller than zero the non-minimum phase system is instabile for the choosen pairings.\n');
end