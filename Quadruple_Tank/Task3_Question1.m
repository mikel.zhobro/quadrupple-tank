%% Question 1 H-inf loop-shaping

s= tf('s');
% non min phase system's weights
W1=(0.8*(s+0.5))^2/(s+0.005)^2;
W2=0.8*(s+0.01)/(s/17+0.1);  
% non min phase system's weights
W1_non=(0.8*(s+0.09)/(s+0.0009))^2;
W2_non=0.8*(s+0.001)/(s/7+0.01); 
bodemag(W2,W2_non)
legend
% figure
%  bodemag(W2,W2_non)
%  legend
 hold off
 %% tunning
% % %  close
% % % W1=(0.8*(s+0.5))^2/(s+0.0075)^2;
% % % W2=0.8*(s+0.03)/(s/17+0.3);    
% % % % non min phase system's weights
% % % W1_non=(0.8*(s+0.02)/(s+0.0003))^2;
% % % W2_non=0.8*(s+0.001)/(s/7+0.01); 
% % % 
% % %   bodemag(W1,W1_non)
% % %   legend

%%
% performance weight
WP=0.4/(s+0.1);
WP_non=0.004/(s+0.001);
bodemag(WP,WP_non)