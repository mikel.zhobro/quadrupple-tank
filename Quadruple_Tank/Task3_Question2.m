%% Question 2 H-inf mixed sensivity prerequisites
%% a)min phase system
[K_inf,CL,GAM]=mixsyn(G,W1,[],W2)
S = feedback(eye(2),G*K_inf);
R = K_inf*S;
T = eye(2)-S;
%%
sigma(S,'b',T,'r',1/W1,'b-.',1/ss(W2),'r-.')
grid on
legend('S','T','GAM/W1','GAM/W2','Location','SouthWest')
    %% plot response
    t=0:0.1:350; t=t';
    u=ones(3501,1);
    u(1:1)=0;
    y=lsim(feedback(G*K_inf,eye(2)),1*[u,u],t,zeros(17,1));
    figure(1)
    subplot(2,2,1)
    plot(t,y(:,1)');
    hold on
    subplot(2,2,2)
    hold on
    plot(t,y(:,2)');
   %% simulink
%     subplot(2,2,1)
%     plot(h12_min_mixed.Time,h12_min_mixed.Data(:,1))
%     subplot(2,2,2)
%     plot( h12_min_mixed.Time,h12_min_mixed.Data(:,2))
%% b)non-min phase system
[K_inf_non,CL_non,GAM_non]=mixsyn(G_non,W1_non,[],W2_non);
S_non = feedback(eye(2),G_non*K_inf_non);
R_non = K_inf_non*S_non;
T_non = eye(2)-S_non;
%%
sigma(S_non,'b',R_non,'r',GAM_non/W1_non,'b-.',GAM_non/ss(W2_non),'r-.')%,GAM/ss(W3_non),'g-.')
grid on
legend('S_non','R_non','GAM/W1_non','GAM/W2_non','GAM/W2_non','Location','SouthWest')
 
%% plot response
    t=0:0.1:1000; t=t';
    u=ones(10001,1);
    u(1:1)=0;
    y=lsim(feedback(G_non*K_inf_non,eye(2)),1*[u,u],t,zeros(18,1),'r');
    subplot(2,2,3)
    plot(t,y(:,1)');
    hold on
    subplot(2,2,4)
    plot(t,y(:,2)')
    hold on
    %% simulink
%     subplot(2,2,3)
%     plot(h12_non_min_mixed.Time,h12_non_min_mixed.Data(:,1))
%     subplot(2,2,4)
%     plot( h12_non_min_mixed.Time,h12_non_min_mixed.Data(:,2))