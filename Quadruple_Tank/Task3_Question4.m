%% Question 4 Robust performance
%% 1)min phase
    S=feedback(eye(2),Gss*K_inf);
    T=eye(2)-S;
    Task3_Question4_min_phase
%% 2)non min phase
    S_non=feedback(eye(2),Gss_non*K_inf_non);
    T_non=eye(2)-S_non;
    Task3_Question4_non_min_phase
%% 3)decentralized
    S_dec = feedback(eye(2),G*K);T_dec = eye(2)-S_dec; T_dec=minreal(T_dec);
    S_non_dec = feedback(eye(2),G*K_non);T_non_dec = eye(2)-S_non_dec;T_non_dec=minreal(T_non_dec);
    %% a)min phase
        RS_dec=hinfnorm(wo*T_dec)
        NP_dec=hinfnorm(WP*S_dec)
        RP_dec=hinfnorm([-wo*T_dec wo*T_dec; -WP*S_dec WP*S_dec])
    %% b)no-nmin phase
        RS_non_dec=hinfnorm(wo_non*T_non_dec)
        NP_non_dec=hinfnorm(WP_non*S_non_dec)
        RP_non_dec=hinfnorm([-wo_non*T_non_dec wo_non*T_non_dec; -WP_non*S_non_dec WP_non*S_non_dec])
%% USSV PLOT
%% NP
    figure
    unstruct_ssv(Gssp,K_inf,WP,'b','NP')
    hold on
    unstruct_ssv(Gssp_non,K_inf_non,WP_non,'r','NP')
    unstruct_ssv(Gssp,K,WP,'--b','NP')
    unstruct_ssv(Gssp_non,K_non,WP_non,'--r','NP')
%% RS
    figure
    unstruct_ssv(Gssp,K_inf,WP,'b','RS')
    hold on
    unstruct_ssv(Gssp_non,K_inf_non,W1_non,'r','RS')
    unstruct_ssv(Gssp,K,WP,'--b','RS')
    unstruct_ssv(Gssp_non,K_non,WP_non,'--r','RS')
%% RP
    figure
    unstruct_ssv(Gssp,K_inf,WP,'b','RP')
    hold on
    unstruct_ssv(Gssp_non,K_inf_non,WP_non,'r','RP')
    unstruct_ssv(Gssp,K,WP,'--b','RP')
    unstruct_ssv(Gssp_non,K_non,WP_non,'--r','RP')

%     %ussv
%     [M,DELTA]=lftdata(W1_non*feedback(eye(2),Gssp_non*K_inf_non));
%     N11_nono=M(1:8,1:8);
%     bounds=mussv(N11_nono,[-1*ones(8,1) zeros(8,1)])
%     mu_RP = bounds ( 1 ) ;
%     bodemag ( mu_RP  )