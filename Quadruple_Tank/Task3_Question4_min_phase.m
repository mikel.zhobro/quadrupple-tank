%% generalized plant
P11=zeros(2); P12=zeros(2); P13=wo*Gss; 
P21=-WP*eye(2); P22=WP*eye(2); P23=-WP*Gss;
P31=-1*eye(2); P32=1*eye(2); P33=-Gss;

% P=minreal([P11 P12 P13; P21 P22 P23; P31 P32 P33; P41 P42 P43]);
P=minreal([P11 P12 P13; P21 P22 P23; P31 P32 P33]);
N=minreal(lft(P,K_inf));

%% nominal stability
if pole(K_inf)>=0
    fprintf('K_inf has RHP pole')
end
if pole(Gss)>=0
    fprintf('G has RHP pole')
end % since there is no rhp zero-pole cancellation system is nominally stable if one loop is stable

S = feedback(eye(2),Gss*K_inf);
if pole(S)<0
    fprintf('System is nominally stable\n')
end
%% roust stability
N11=N(1:2,1:2);
RS=hinfnorm(N11) 
%hinfnorm(wo*T)
%% nominal performance
N22=N(3:end,3:end);
NP=hinfnorm(N22) 
%hinfnorm(WP*S)
%% robust performance
RP=hinfnorm(N)  
%hinfnorm([-wo*T wo*T; -WP*S WP*S])