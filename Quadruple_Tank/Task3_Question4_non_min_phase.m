%% generalized plant
P11_non=zeros(2); P12_non=zeros(2); P13_non=wo_non*Gss_non; 
P21_non=-WP_non*eye(2); P22_non=WP_non*eye(2); P23_non=-WP_non*Gss_non;
P31_non=-1*eye(2); P32_non=1*eye(2); P33_non=-Gss_non;

% P=minreal([P11 P12 P13; P21 P22 P23; P31 P32 P33; P41 P42 P43]);
P_non=minreal([P11_non P12_non P13_non; P21_non P22_non P23_non; P31_non P32_non P33_non]);
N_non=minreal(lft(P_non,K_inf_non));

%% nominal stability
if pole(K_inf_non)>=0
    fprintf('K_inf has RHP pole')
end
if pole(Gss_non)>=0
    fprintf('G has RHP pole')
end % since there is no rhp zero-pole cancellation system is nominally stable if one loop is stable

if pole(S_non)<0
    fprintf('System is nominally stable\n')
end
%% robust stability
N11_non=N_non(1:2,1:2); 
RS_non=hinfnorm(N11_non) 
%RS_non=hinfnorm(wo_non*T_non)
%% nominal performance
N22_non=N_non(3:end,3:end);
%NP_non=hinfnorm(N22_non) 
NP_non=hinfnorm(WP_non*S_non)
%% robust performance
% RP_non=hinfnorm(N_non)   
RP_non=hinfnorm([-wo_non*T_non wo_non*T_non; -WP_non*S_non WP_non*S_non])