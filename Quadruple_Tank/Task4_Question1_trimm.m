%% Question 1: Nonlinear model for leckage
clear all;

% Initialise parameters
init_all;
Ad3=0.1;
Ad4=0.1;

% Trim point input limitation and optimisation settings
op = operspec('h_inf_loop_nonlinear');
inputs = get(op.Inputs);
% Limit input
set(op.Inputs(1), 'Min', 0);
set(op.Inputs(2), 'Min', 0); % set(op.Inputs(2), 'Min', 0, 'Max', 1);
% Limit state vectors
set(op.State(1), 'Known', 1);
set(op.State(2), 'Known', 1);
set(op.State(3), 'Known', 1);
set(op.State(4), 'Known', 1);

% Trim model
[op_point, op_report] = findop('h_inf_loop_nonlinear', op);

% Get trim results
U  = [op_point.Inputs(1).u; op_point.Inputs(2).u];
DX = [op_report.States(1).dx; op_report.States(2).dx; op_report.States(3).dx; op_report.States(4).dx];
X  = [op_report.States(1).x; op_report.States(2).x; op_report.States(3).x; op_report.States(4).x];
Y  = [op_report.Outputs(1).y; op_report.Outputs(2).y];

% Linearise model
[An, Bn, Cn, Dn] = linmod('h_inf_loop_nonlinear', X, U);
