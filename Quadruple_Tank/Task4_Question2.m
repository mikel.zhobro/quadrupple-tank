%% Question 2: Linearization for leckage

%%
A4=32; % [cm^2]

% Cross sectional area of outlet
a1=0.071; % [cm^2]
a2=0.057; % [cm^2]
a3=0.071; % [cm^2]
a4=0.057; % [cm^2]

% Graviation force
g=981; % [cm/s^2]

% Model Parameters for minimum phase system
h10=7.586;
h20=7.948;
h30=0.281;
h40=0.185;

% Initial voltage of pump
v10=3; % [V]
v20=3; % [V]

% Conversion gain of pump voltage to flow 
k1=3.33; % [cm^3/Vs]
k2=3.35; % [cm^3/Vs]

% Ratio of flow to observed tank and flow of pump
g1=0.70; % [-]
g2=0.60; % [-]

% Parameters of leakages:
Ad30=0.1; % [cm^2]
Ad40=0.1; % [cm^2]

a11=-a1/A1*sqrt(g/2/h10);
a13=a3/A1*sqrt(g/2/h30);
a22=-a2/A2*sqrt(g/2/h20);
a24=a4/A2*sqrt(g/2/h40);
a33=-(a3+Ad30)/A3*sqrt(g/2/h30);
a44=-(a4+Ad40)/A4*sqrt(g/2/h40);

A=[a11 0 a13 0;0 a22 0 a24;0 0 a33 0;0 0 0 a44];

b11=g1*k1/A1;
b22=g2*k2/A2;
b32=(1-g2)*k2/A3;
b41=(1-g1)*k1/A4;

B=[b11 0;0 b22;0 b32;b41 0];

C=[1 0 0 0;
   0 1 0 0;];

D=zeros(2,2);


Gss_lek=ss(A,B,C,D);
