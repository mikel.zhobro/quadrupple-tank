 %% plot response
    t=0:0.1:350; t=t';
    u=ones(3501,1);
    u(1:1)=0;
    y=lsim(feedback(G*K_inf,eye(2)),1*[u,u],t,zeros(17,1));
%     figure(1)
    subplot(1,2,1)
    plot(t,y(:,1)');
    hold on
    subplot(1,2,2)
    hold on
    plot(t,y(:,2)');
   %% simulink
    subplot(1,2,1)
    plot(h1_min_mixed.Time,h1_min_mixed.Data(:,1))
    subplot(1,2,2)
    plot( h2_min_mixed.Time,h2_min_mixed.Data(:,1))
    
   