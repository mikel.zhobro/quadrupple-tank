%% Question 3: Controller design for leckage

%% min phase
W1=0.8/(s*(s+25));
% W1=0.5/(s*(s+30));
W2 = 1;
[K_lek,CL_lek,GAM_lek,info_lek] = ncfsyn(Gss_lek,W1,W2);
K_lek=-K_lek;
sigma(Gss_lek*K_lek,'r',Gss_lek*W1,'r-.',Gss_lek*W1*GAM_lek,'k-.',Gss_lek*W1/GAM_lek,'k-.')
L_lek=Gss_lek*K_lek;
S_lek = feedback(eye(2),L_lek); % S=inv(I+L);
T_lek = eye(2)-S_lek;
sigma(Gss_lek*K_lek,'r',Gss_lek,'r-.')
% figure;
sigma(T_lek,eye(2)+L_lek,'r--',{.01,100})
legend('\sigma(T) robustness','1/\sigma(S) performance')


