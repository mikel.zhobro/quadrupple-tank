%% Question 4: Evaluation of disturbance rejection for leckage
WP = (0.2*s+0.1)/(s+0.2);
Task4_Question4_uncertainty

%% USSV PLOT
%% NP
    figure
    unstruct_ssv(Gssp_lek,K_lek,WP,'b','NP')
    %% RS
    figure
    unstruct_ssv(Gssp_lek,K_lek,WP,'b','RS')
%% RP
    figure
    unstruct_ssv(Gssp_lek,K_lek,WP,'b','RP')