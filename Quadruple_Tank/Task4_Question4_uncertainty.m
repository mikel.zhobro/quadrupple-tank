%% uncertainty on the plant with lekage
%% Question 2
a1_p =a1; a1_p= ureal ( 'a1', a1_p, 'Range', [0.95*a1_p, 1.05*a1_p] );
a2_p =a2; a2_p = ureal ( 'a2', a2_p, 'Range', [0.95*a2_p, 1.05*a2_p] );
a3_p =a3; a3_p = ureal ( 'a3', a3_p, 'Range', [0.95*a3_p, 1.05*a3_p] );
a4_p =a4; a4_p = ureal ( 'a4', a4_p, 'Range', [0.95*a4_p, 1.05*a4_p] );

% Parameters of min phase operating point 
g1_p=g1; g1_p = ureal ( 'g1', g1_p, 'Range', [0.9*g1_p, 1.1*g1_p] );
g2_p=g2; g2_p = ureal ( 'g2', g2_p, 'Range', [0.9*g2_p, 1.1*g2_p] );
k1_p=k1; k1_p = ureal ( 'k1', k1_p, 'Range', [0.9*k1_p, 1.1*k1_p] );
k2_p=k2; k2_p = ureal ( 'k2', k2_p, 'Range', [0.9*k2_p, 1.1*k2_p] );


%%
a11=-a1_p/A1*sqrt(g/2/h10);
a13=a3_p/A1*sqrt(g/2/h30);
a22=-a2_p/A2*sqrt(g/2/h20);
a24=a4_p/A2*sqrt(g/2/h40);
a33=-(a3_p+Ad30)/A3*sqrt(g/2/h30);
a44=-(a4_p+Ad40)/A4*sqrt(g/2/h40);

Ap_lek=[a11 0 a13 0;0 a22 0 a24;0 0 a33 0;0 0 0 a44];

b11=g1_p*k1_p/A1;
b22=g2_p*k2_p/A2;
b32=(1-g2_p)*k2_p/A3;
b41=(1-g1_p)*k1_p/A4;

Bp_lek=[b11 0;0 b22;0 b32;b41 0];

C=[1 0 0 0;
   0 1 0 0;];

D=zeros(2,2);

%%
Gssp_lek=ss(Ap_lek,Bp_lek,C,D);
Gp_lek= tf(Gssp_lek);