%% Question 5: Evaluation of controller based on leckage


    %% simulink nonlinear
    % disturbance on plants output
%     subplot(2,2,4)
%     plot( u_min_mixed.Time,u_min_mixed.Data(:,1))
%     hold on
%     plot( u_min_mixed.Time,u_min_mixed.Data(:,2))
%     plot( u_min_mixed.Time,u_min_mixed.Data(:,3))
       
    %% linear
%    %% freq=10rad/s
%     t=0:0.1:10; t=t';
%     u=sin(10*t);
%     y=lsim(Gss_lek*feedback(eye(2),Gss_lek*K_lek),1*[u,u],t,zeros(19,1));
%     subplot(2,4,3)
%     plot(t,y(:,1)');
%     subplot(2,4,4)
%     plot(t,y(:,2)');

%    %% freq=1rad/s
%     t=0:0.1:1000; t=t';
%     u=sin(t);
%     y=lsim(Gss_lek*feedback(eye(2),Gss_lek*K_lek),1*[u,u],t,zeros(19,1));
%     figure(1)
%     subplot(2,4,1)
%     plot(t,y(:,1)');
%     subplot(2,4,2)
%     plot(t,y(:,2)');

%     %% freq=0.1rad/s
%     t=0:0.1:4000; t=t';
%     u=1*sin(0.1*t);
%     y=lsim(Gss_lek*feedback(eye(2),Gss_lek*K_lek),1*[u,u],t,zeros(19,1));
%     subplot(2,4,5)
%     plot(t,y(:,1)');
%     subplot(2,4,6)
%     plot(t,y(:,2)');

%     %%freq=0.01rad/s
%     t=0:0.1:4000; t=t';
%     u=0.5*sin(0.01*t);
%     y=lsim(Gss_lek*feedback(eye(2),Gss_lek*K_lek),1*[u,u],t,zeros(19,1));
%     subplot(2,4,7)
%     plot(t,y(:,1)');
%     subplot(2,4,8)
%     plot(t,y(:,2)');

