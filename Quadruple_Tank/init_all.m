% Initialization of parameters in quadruple tank system

% System Parameters
% Cross sectional area of tank 
A1=28; % [cm^2]
A2=32; % [cm^2]
A3=28; % [cm^2]
A4=32; % [cm^2]

% Cross sectional area of outlet
a1=0.071; % [cm^2]
a2=0.057; % [cm^2]
a3=0.071; % [cm^2]
a4=0.057; % [cm^2]

% Graviation force
g=981; % [cm/s^2]

%states outputs etc
states = { 'h_1' 'h_2' 'h_3' 'h_4'};
input = { 'u_1' 'u_2' };
outputs = { 'h_1'; 'h_2' };
min_phase=1;
%% Model Parameters for minimum phase system
% Initial height of water level in the tank
if min_phase
    h10=12.4; % [cm]
    h20=12.7; % [cm]
    h30=1.8; % [cm]
    h40=1.4; % [cm]

    % Initial voltage of pump
    v10=3; % [V]
    v10=3; % [V]

    % Conversion gain of pump voltage to flow 
    k1=3.33; % [cm^3/Vs]
    k2=3.35; % [cm^3/Vs]

    % Ratio of flow to observed tank and flow of pump
    g1=0.70; % [-]
    g2=0.60; % [-]
end
if ~min_phase
    h10=12.6; % [cm]
    h20=13.0; % [cm]
    h30=4.8; % [cm]
    h40=4.9; % [cm]

    % Initial voltage of pump
    v10=3.00; % [V]
    v10=3.00; % [V]

    % Conversion gain of pump voltage to flow 
    k1=3.33; % [cm^3/Vs]
    k2=3.35; % [cm^3/Vs]

    % Ratio of flow to observed tank and flow of pump
    g1=0.70; % [+]
    g2=0.60; % [+] 
end
a11=-a1/A1*sqrt(g/2/h10);
a13=a3/A1*sqrt(g/2/h30);
a22=-a2/A2*sqrt(g/2/h20);
a24=a4/A2*sqrt(g/2/h40);
a33=-a3/A3*sqrt(g/2/h30);
a44=-a4/A4*sqrt(g/2/h40);

A=[a11 0 a13 0;0 a22 0 a24;0 0 a33 0;0 0 0 a44];

b11=g1*k1/A1;
b22=g2*k2/A2;
b32=(1-g2)*k2/A3;
b41=(1-g1)*k1/A4;

B=[b11 0;0 b22;0 b32;b41 0];

C=eye(4);

D=zeros(4,2);

% Initialization of state variables
X=[h10; h20; h30; h40];

%% Model Parameters for nom-minimum phase system
% Initial height of water level in the tank
    h10_non=12.6; % [cm]
    h20_non=13.0; % [cm]
    h30_non=4.8; % [cm]
    h40_non=4.9; % [cm]

    % Initial voltage of pump
    v10_non=3.15; % [V]
    v10_non=3.15; % [V]

    % Conversion gain of pump voltage to flow 
    k1_non=3.14; % [cm^3/Vs]
    k2_non=3.29; % [cm^3/Vs]

    % Ratio of flow to observed tank and flow of pump
    g1_non=0.43; % [+]
    g2_non=0.34; % [+] 
    
    a11_non=-a1/A1*sqrt(g/2/h10_non);
    a13_non=a3/A1*sqrt(g/2/h30_non);
    a22_non=-a2/A2*sqrt(g/2/h20_non);
    a24_non=a4/A2*sqrt(g/2/h40_non);
    a33_non=-a3/A3*sqrt(g/2/h30_non);
    a44_non=-a4/A4*sqrt(g/2/h40_non);

    A_non=[a11_non 0 a13_non 0;0 a22_non 0 a24_non;0 0 a33_non 0;0 0 0 a44_non];

    b11_non=g1_non*k1_non/A1;
    b22_non=g2_non*k2_non/A2;
    b32_non=(1-g2_non)*k2_non/A3;
    b41_non=(1-g1_non)*k1_non/A4;

    B_non=[b11_non 0;0 b22_non;0 b32_non;b41_non 0];
    C_non=[1 0 0 0; 0 1 0 0];
    D_non=zeros(2,2);
    
    % Initialization of state variables
    X_non=[h10_non; h20_non; h30_non; h40_non];
%% TF and State space systems
    Gss=ss(A,B,C_non,D_non,'statename', states ,'inputname', input ,'outputname', outputs );
    Gss_non=ss(A_non,B_non,C_non,D_non,'statename', states ,'inputname', input ,'outputname', outputs );
    G=tf(Gss);
    G_non=tf(Gss_non);