function [] = unstruct_ssv(Gssp,K,W1,col,X)
%UNTITLED4 Print ussv over frequency for robust stability
%   ussv of W1*S for diagonal parametric uncertainty 
if X=='RS'
    [M,DELTA]=lftdata(W1*feedback(eye(2),Gssp*K));
    [~,n]=size(DELTA);
    N11=M(1:n,1:n);
    N11fr = frd ( N11 , logspace ( -3 , 3 , 100 ) ) ;
    bounds=mussv(N11fr,[-1*ones(n,1) zeros(n,1)]);
    mu_RS = bounds ( 1 ) ;
    sigma ( mu_RS ,col )
end
if X=='RP'
    [M,DELTA]=lftdata(W1*feedback(eye(2),Gssp*K));
    [~,n]=size(DELTA);
    [~,m]=size(M);
    Mfr = frd ( M , logspace ( -3 , 3 , 100 ) ) ;
    bounds=mussv(Mfr,[-1*ones(n,1) zeros(n,1);[m-n,m-n]]);
    mu_RP = bounds ( 1 ) ;
    bodemag ( mu_RP ,col )
end
if X=='NP'
    [M,DELTA]=lftdata(W1*feedback(eye(2),Gssp*K));
    [~,n]=size(DELTA);
    [~,m]=size(M);
    N22=M(n+1:end,n+1:end);
    N22fr = frd ( N22 , logspace ( -3 , 3 , 100 ) ) ;
    bounds=mussv(N22fr,[m-n,m-n]);
    mu_RS = bounds ( 1 ) ;
    bodemag ( mu_RS ,col )
end
end

% N22fr = frd ( N22 , logspace ( -3 , 3 , 100 ) ) ;
% bounds = mussv ( N22fr , [ 2 2 ] ) ;